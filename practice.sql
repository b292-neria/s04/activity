CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	contact_number INT NOT NULL,
	email VARCHAR(50),
	address VARCHAR(50),
	PRIMARY KEY (id)
);

INSERT INTO users(username, password, full_name, contact_number, email, address) 
VALUES ("ezrealAD", "ez", "Jarro Lightfeather", 2147483647, "ez@league.com", "Piltover"),
	("seraphine", "password123", "Seraphine", 2147483647, "seraphine@riot.com", "Ionia"),
	("lux", "lux123", "Luxanna Crownguard", 2147483647, "lux@demacia.com", "Demacia"),
	("viktor", "1234viktor", "Viktor", 2147483647, "viktor@zaun.com", "Zaun"),
	("jinx", "jinx123", "Jinx", 2147483647, "jinx@zaun.com", "Zaun");


CREATE TABLE reviews (
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	review VARCHAR(300) NOT NULL,
	datetime_created DATETIME NOT NULL,
	rating INT NOT NULL,
	PRIMARY KEY (id),
		CONSTRAINT fk_reviews_user_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

INSERT INTO reviews(user_id, review, datetime_created, rating) VALUES (1, "The songs are okay. Worth the subscription", "2023-05-03 00:00:00", 5), 
	(2, "The songs are meh. I want BLACKPINK", "2023-01-23 00:00:00", 1), 
	(3, "Add Bruno Mars and Lady Gaga", "2023-03-23 00:00:00", 4),
	(4, "I want to listen to more k-pop", "2022-09-23 00:00:00", 3), 
	(5, "Kindly add more OPM", "2023-02-01 00:00:00", 5) ;


SELECT * FROM users
	JOIN reviews ON users.id = reviews.user_id
	WHERE users.username LIKE "%k%";
SELECT * FROM users
	JOIN reviews ON users.id = reviews.user_id
	WHERE users.username LIKE "%x%";
SELECT * FROM reviews
	JOIN users ON users.id = reviews.user_id;
SELECT reviews.review, users.username FROM reviews
	JOIN users ON users.id = reviews.user_id;
